"use strict";

const OAUTH = require('oauth2-server');
const MODEL = require('./model');

const OPTIONS = {
  model: MODEL.model,
  grants: MODEL.grants,
  debug: true,
  continueAfterResponse: false
};

let oauth = new OAUTH(OPTIONS);

module.exports = oauth;
