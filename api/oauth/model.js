"use strict";

const DB = require('../db');

const GRANTS = ['password', 'refresh_token', 'client_credentials'];

const MODEL = {
  getAccessToken: (bearerToken, cb) => {
    return DB.findToken({accessToken: bearerToken}, true)
    .then(data => cb(false, data))
    .catch(e => cb(e));
  },
  getClient: (clientId, clientSecret, cb) => {
    return DB.findClient({
      clientId: clientId,
      clientSecret: clientSecret
    })
    .then(data => cb(false, data))
    .catch(e => cb(e));
  },
  grantTypeAllowed: (clientId, grantType, cb) => {
    return DB.grantTypeAllowed({clientId: clientId}, grantType)
    .then(data => cb(false, data))
    .catch(e => cb(e));
  },
  saveAccessToken: (accessToken, clientId, expires, user, cb) => {
    return DB.saveToken({
      accessToken: accessToken,
      accessTokenExpiresOn: expires
    }, clientId, user)
    .then(data => cb(false, data))
    .catch(e => cb(e));
  },

  // --- authorization_code grant methods --- //
  // getAuthCode: authCode => {},
  // saveAuthCode: (authCode, clientId, expires, user) => {}

  // --- password grant methods --- //
  getUser: (username, password, cb) => {
    return DB.findUser({
      username: username,
      password: password
    })
    .then(data => cb(false, data))
    .catch(e => cb(e));
  },

  // --- refresh_token grant methods --- //
  saveRefreshToken: (refreshToken, clientId, expires, user, cb) => {
    return DB.saveToken({
      refreshToken: refreshToken,
      refreshTokenExpiresOn: expires
    }, clientId, user)
    .then(data => cb(false, data))
    .catch(e => cb(e));
  },
  getRefreshToken: (refreshToken, cb) => {
    return DB.findToken({refreshToken: refreshToken}, false)
    .then(data => cb(false, data))
    .catch(e => cb(e));
  },
  revokeRefreshToken: (refreshToken, cb) => {
    return DB.revokeRefreshToken({refreshToken: refreshToken})
    .then(data => cb(false, data))
    .catch(e => cb(e));
  },

  // --- client_credentials grant methods --- //
  getUserFromClient: (clientId, clientSecret, cb) => {
    return DB.findUserByClient({
      clientId: clientId,
      clientSecret: clientSecret
    })
    .then(data => cb(false, data))
    .catch(e => cb(e));
  }
};

module.exports = {
  model: MODEL,
  grants: GRANTS
};
