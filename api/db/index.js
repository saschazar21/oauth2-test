"use strict";

const Sequelize = require('sequelize');
const PATH = require('path');
const CRYPTO = require('crypto');

const OPTIONS = {
  storage: PATH.resolve(__dirname, '../../db.sqlite3')
};

/*
 * Database class.
 * Handles persistence of users, clients and tokens
 */
class Database {
  constructor() {
    this._db = new Sequelize('sqlite:db.sqlite3', OPTIONS);
  }
  set user(model) {
    return this._user = model;
  }
  set client(model) {
    return this._client = model;
  }
  set token(model) {
    return this._token = model;
  }
  get db() {
    return this._db;
  }
  get user() {
    return this._user;
  }
  get client() {
    return this._client;
  }
  get token() {
    return this._token;
  }

  // Create database relations:
  // A user might have many clientIds,
  // a client has only one token pair
  createRelations() {
    this.user.hasMany(this.client);
    this.client.hasOne(this.token);
    return this.db.sync();
  }

  // Create a new user,
  // create a new client as well
  saveUser(opts) {
    if (!opts.username || !opts.password) {
      return null;
    }
    return this.user.create(opts)
    .then(u => u.createClient({
      clientId: CRYPTO.createHash('sha256').update(u.username + u.createdAt).digest('hex').substr(0,20),
      clientSecret: CRYPTO.createHash('sha256').update(Date.now() + u.password).digest('hex').substr(0,27),
      redirectUri: 'http://localhost:' + (process.env.PORT || 3000) + '/login'
    }))
    .then(c => {
      let oauthHeader = new Buffer(c.clientId + ':' + c.clientSecret).toString('base64');
      return {
        client_id: c.clientId,
        client_secret: c.clientSecret,
        oauth: oauthHeader
      };
    });
  }
  findUser(opts) {
    return this.user.findOne({
      where: opts
    })
    .then(d => {
      return {
        id: d.id,
        username: d.username
      }
    });
  }
  findUserByClient(opts) {
    return this.client.findOne({
      where: opts
    })
    .then(d => this.findUser({id: d.userId}));
  }
  findClient(opts) {
    if (opts.clientSecret === null) {
      delete opts.clientSecret;
    }
    return this.client.findOne({
      where: opts
    })
    .then(d => {

      // Needed by oauth2-server@3.0.0-b2,
      // don't really know if necessary in ^2.4.1
      let grants = [];
      if (d.password) grants.push('password');
      if (d.authorization_code) grants.push('authorization_code');
      if (d.refresh_token) grants.push('refresh_token');
      if (d.client_credentials) grants.push('client_credentials');

      return {
        grants: grants,
        client_id: d.clientId,
        redirect_uri: d.redirectUri
      };
    });
  }
  findToken(opts, checkAccessTokenExpiry) {
    return this.client.findOne({
      include: [{
        model: this.token,
        where: opts
      }]
    })
    .then(d => {
      if (checkAccessTokenExpiry && new Date(d.token.accessTokenExpiresOn).getTime() < Date.now()) {
        throw new Error('Access Token expired.');
      } else {
        return d;
      }
    })
    .then(d => {
      return {
        client_id: d.clientId,
        expires: new Date(d.token.accessTokenExpiresOn).toISOString(),
        user_id: d.userId
      };
    });
  }

  // Save generated token to database
  // First retrieve the correct client,
  // then update the token data
  saveToken(opts, clientId, user) {
    let client;

    return this.client.findOne({
      where: {clientId: clientId}
    })
    .then(d => {
      client = d;
      return d.getToken()
    })
    .then(t => t.update(opts))
    .catch(() => client.createToken(opts))
    .catch(e => {
      console.log('Token creation failed: ', e)
    });
  }
  revokeRefreshToken(opts) {
    return this.token.findOne({
      where: opts
    })
    .then(t => t.update({refreshTokenExpiresOn: new Date(Date.now() - 3600)}))
  }
  grantTypeAllowed(opts, grant) {
    return this.client.findOne({
      where: opts
    })
    .then(d => {
      if (!d[grant]) {
        throw new Error('Grant type not allowed for this client ID');
      } else {
        return true;
      }
    });
  }
}

//---- Creating instance ----//

const DB = new Database();

DB.user = DB.db.define('user', {
  username: Sequelize.STRING,
  password: Sequelize.STRING
});

DB.client = DB.db.define('client', {
  clientId: Sequelize.STRING,
  clientSecret: Sequelize.STRING,
  redirectUri: Sequelize.STRING,
  password: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: true
  },
  authorization_code: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false
  },
  refresh_token: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: true
  },
  client_credentials: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: true
  }
});

DB.token = DB.db.define('token', {
  accessToken: Sequelize.STRING,
  accessTokenExpiresOn: Sequelize.DATE,
  refreshToken: Sequelize.STRING,
  refreshTokenExpiresOn: Sequelize.DATE
});

DB.createRelations().
catch(e => console.log('Something went wrong while creating the database!', e));

module.exports = DB;
