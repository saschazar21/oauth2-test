"use strict";

var express = require('express');
var router = express.Router();
var oauth = require('../api/oauth');

/* POST to get oauth token.
 *
 * Body must contain a grant_type field (available grant_types in /api/oauth/index.js)
 * Header must contain 'Authorization: Basic ' + clientId:clientSecret encoded in base64
 */
router.post('/token', oauth.grant());
// return value should be something like this:
//
// {
//   "token_type": "bearer",
//   "access_token": "0e3f711a120d039ab722c3674560976d02de2d52",
//   "expires_in": 3600,
//   "refresh_token": "65a2a32d2aa6f402b84a46ff8c64910a4f00a3a5"
// }

module.exports = router;
