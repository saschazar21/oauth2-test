var express = require('express');
var router = express.Router();
const DB = require('../api/db');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/register', function(req, res, next) {
  let userCreate = DB.saveUser(req.body);
  if (userCreate === null) {
    return res.status(400).json({error: "Wrong data given. username and/or password missing."});
  }
  return userCreate
  .then(u => res.json(u))
  .catch(e => res.status(500).json({
    error: "Something went wrong while creating the user.",
    stack: e
  }));
});

module.exports = router;
